﻿//
// server.hpp
// ~~~~~~~~~~
//
// Main Sever Connection provide class
//

#ifndef BOOSTCONNECT_SERVER_HPP
#define BOOSTCONNECT_SERVER_HPP

#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/noncopyable.hpp>
#include "manager.hpp"
#include "session/session_base.hpp"
#include "session/http_session.hpp"

namespace bstcon{

class server : boost::noncopyable
{
private:
    typedef session::http_session SessionType;
    manager<session::session_base> manage_;

public:
    typedef boost::asio::io_service io_service;
    typedef boost::system::error_code error_code;

	/// The handler for all incoming requests.
    typedef session::http_session::RequestHandler RequestHandler;

	
    typedef session::http_session::CloseHandler CloseHandler;

	server(io_service &io_service, unsigned short port, std::size_t thread_pool_size);

#ifdef USE_SSL_BOOSTCONNECT
    typedef boost::asio::ssl::context context;
	server(io_service &io_service,context &ctx,unsigned short port, std::size_t thread_pool_size);
#endif

    void start(RequestHandler handler);
private:	
	
	/// Handle completion of an asynchronous accept operation.
    void handle_accept(boost::shared_ptr<session::session_base> new_session,
					   RequestHandler handler,
					   const boost::system::error_code& ec);

	/// Handle a request to stop the server.
    void handle_closed(boost::shared_ptr<session::session_base>& session);	

	/// Initiate an asynchronous accept operation. 
	void start_accept(RequestHandler handler, boost::shared_ptr<SessionType> new_session);

	/// Handle a request to stop the server.
	void handle_stop();	

private:		
	/// The io_service used to perform asynchronous operations.
	boost::asio::io_service* io_service_;

	/// The signal_set is used to register for process termination notifications.
	boost::asio::signal_set signals_;

	/// Acceptor used to listen for incoming connections.
    boost::asio::ip::tcp::acceptor acceptor_;

	/// The number of threads that will call io_service::run().
	std::size_t thread_pool_size_;
	
    const unsigned short port_;
	
    bool is_ctx_;
	
#ifdef USE_SSL_BOOSTCONNECT
    context *ctx_;
#endif
};

} // namespace bstcon

#ifdef BOOSTCONNECT_LIB_BUILD
#include "impl/server.ipp"
#endif

#endif
