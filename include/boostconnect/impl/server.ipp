﻿//
// server.ipp
// ~~~~~~~~~~
//
// Main Sever Connection provide class
//

#ifndef BOOSTCONNECT_SERVER_IPP
#define BOOSTCONNECT_SERVER_IPP

#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/noncopyable.hpp>
#include <boost/thread/thread.hpp>
#include "../server.hpp"

namespace bstcon{

	server::server(io_service &io_service,unsigned short port, std::size_t thread_pool_size)
	: io_service_(&io_service),
	signals_(io_service),
    acceptor_(io_service,boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port)),
    thread_pool_size_(thread_pool_size),
    port_(port),
    is_ctx_(false)
{
	// Register to handle the signals that indicate when the server should exit.
	// It is safe to register for the same signal multiple times in a program,
	// provided all registration for the specified signal is made through Asio.
	signals_.add(SIGINT);
	signals_.add(SIGTERM);
	
	#if defined(SIGQUIT)
		signals_.add(SIGQUIT);
	#endif // defined(SIGQUIT)
		
	signals_.async_wait(boost::bind(&server::handle_stop, this));
}

#ifdef USE_SSL_BOOSTCONNECT
typedef boost::asio::ssl::context context;
server::server(io_service &io_service,context &ctx,unsigned short port, std::size_t thread_pool_size)
	: io_service_(&io_service),
	  signals_(io_service),
	  acceptor_(io_service,boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port))
	  port_(port),
	  is_ctx_(true),
	  ctx_(&ctx) 
{
}
#endif

void server::start_accept(RequestHandler handler, boost::shared_ptr<SessionType> new_session)
{
	acceptor_.async_accept(new_session->lowest_layer(),
						   boost::bind(&server::handle_accept,this,
									   new_session,
										handler,
					 boost::asio::placeholders::error));
}

void server::start(RequestHandler handler)
{
    if(!is_ctx_)
    {
        boost::shared_ptr<SessionType> new_session(new SessionType(*io_service_));
		start_accept(handler, new_session);
    }    
#ifdef USE_SSL_BOOSTCONNECT
    else
    {
        boost::shared_ptr<SessionType> new_session(new SessionType(*io_service_, *ctx_));
		start_accept(handler, new_session);
    }
#endif	
	
    return;
}

void server::handle_accept(boost::shared_ptr<session::session_base> new_session,RequestHandler handler,const boost::system::error_code& ec)
{
    start(handler);
    manage_.run(new_session);

    if(!ec)
	{
		new_session->start(handler, boost::bind(&server::handle_closed, this, _1));
	}	
    else
	{
		new_session->end([](boost::shared_ptr<session::session_base>&) -> void { return; });
	}
}

void server::handle_closed(boost::shared_ptr<session::session_base>& session)
{
    manage_.stop(session);
    return;
}

void server::handle_stop()
{
	io_service_->stop();
}

} // namespace bstcon

#endif
